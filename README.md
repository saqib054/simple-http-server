# Users map

React application to show users on google maps

# Notes

You need to have `yarn` and `node` installed in your system

## Installation and Instructions

Clone repo and run `npm install` to install the dependencies. It's only a `json-server`, which serves the data.
